-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th9 12, 2022 lúc 10:22 AM
-- Phiên bản máy phục vụ: 10.4.21-MariaDB
-- Phiên bản PHP: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `QLSV`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `DMKHOA`
--

CREATE TABLE `DMKHOA` (
  `MaKH` varchar(6) NOT NULL,
  `TenKhoa` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `SINHVIEN`
--

CREATE TABLE `SINHVIEN` (
  `MaSV` varchar(6) NOT NULL,
  `HoSV` varchar(30) NOT NULL,
  `TenSV` varchar(15) NOT NULL,
  `GioiTinh` char(1) NOT NULL,
  `NgaySinh` datetime NOT NULL,
  `NoiSinh` varchar(50) NOT NULL,
  `DiaChi` varchar(50) NOT NULL,
  `MaKH` varchar(6) NOT NULL,
  `HocBong` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
